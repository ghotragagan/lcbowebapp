<%@ page import ="java.util.*" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="/css/bootstrap.min.1.4.0.css">
	</head>
	<body>
		<center>
		
		<h1>
		    Available Brands of <% out.println(request.getAttribute("liquourType")); %>
		</h1>
		
		<%
			List result= (List) request.getAttribute("brands");
			
			Iterator it = result.iterator();
			
			out.println("<br>At LCBO, We have <br><br>");
			
			while(it.hasNext()){
				out.println(it.next()+"<br>");
			}
		%>
		<button onclick="goBackToLiquorSelection()">Go Back To Liquor Selection</button>
		<script>
		function goBackToLiquorSelection(){
		    window.history.back();
		}
		</script>
	</body>
</html>